<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 27.04.2016
 */

require_once 'vendor/autoload.php';

use Core\Parser;

try{
    
    Parser::setErrorHandler();
    Parser::run($argv);
    
}catch ( ErrorException $e){
    
    fwrite(STDOUT, 'Error: ' . $e->getMessage());
    
}