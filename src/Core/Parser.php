<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 06.05.2016
 */

namespace Core;

use Core\Extensions\ExportCSV;
use Core\Model\Site;
use ErrorException;

class Parser
{
    use ExportCSV;

    public static function run(array $args)
    {
        if (count($args) < 5) {
            throw new ErrorException ("You did not provide all required arguments");
        }else {
            fwrite(STDOUT, 'Loading Site...' . "\n");
            define('ENT_LIMIT', $args[4]);
            $site = Site::getInstance($args);
            fwrite(STDOUT, $site->getUrl() . ' loaded' . "\n");
            
            $entities = $site->getEntities();
        }
        // TODO::
        foreach ($entities as $entity) {

        }

    }


    public static function setErrorHandler()
    {
        set_error_handler(function($errno, $errstr){
            throw new ErrorException($errstr, 0, $errno);
        }, E_ALL);
    }
    
    // TODO::
    public static function getHelp()
    {
        
    }
}