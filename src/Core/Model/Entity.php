<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 28.04.2016
 */

namespace Core\Model;


abstract class Entity
{
 
    public static function getInstance($data, $site)
    {
        $site = str_replace('Page', '', explode('\\',get_class($site))[2]);
        $class = '\\Model\\Entity\\' . $site . 'Entity';
        return new $class($data);
    }

    
}