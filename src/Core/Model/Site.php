<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 30.04.2016
 */

namespace Core\Model;

use Sunra\PhpSimple\HtmlDomParser as HtmlParser;
use ErrorException;

abstract class Site
{
    /**
     * @var Page        Site page object
     */
    protected $page;

    /**
     * @var HtmlParser  Main parser object
     */
    protected $parser;

    /**
     * @var array       Collection of site pages urls
     */
    protected $paginator;
    
    /**
     * @var array       Site main sections array
     */
    protected $sections;

    /**
     * @var string      current site section
     */
    protected $section;

    /**
     * @var string      Site main url
     */
    protected $url;

    /**
     * @var array       entities objects array
     */
    protected $entities;

   
    public function __construct($action, $option)
    {
        $this->parser = new HtmlParser();                               // Create main Parser object

        if ( (array_key_exists($action, $this->sections)) && (array_key_exists($option, $this->sections[$action])) ) {
            $this->section = $this->sections[$action][$option];
        }else {
            throw new ErrorException('Action or Option provided does not exists for this site');
        }
        
        $this->page = $this->loadPage($this->url . $this->section);     // Load initial section page
        $this->paginator = $this->page->loadPaginator();                // load paginator urls
    }

    /**
     * Returns new instance of Site
     * @param $args
     * @return object
     * @throws ErrorException
     */
    public static function getInstance($args)
    {
        $class = "Model\\Site\\".ucfirst($args[1]).'Site';
        if (class_exists($class)) {
            return new $class($args[2], $args[3]);
        }else {
            throw new ErrorException ('Site does not exists');
        }

    }

    /**
     * Traverse all urls from paginator array
     * Loads all entities from site until limit is reached
     * @return array    entitites objects array
     */
    public function getEntities()
    {
        if (empty($this->entities)) {
            while (count($this->entities) < ENT_LIMIT) {
                if($page_url = next($this->paginator)){
                    $this->loadEntities();                                     // parse all entities from current page
                    $this->page = $this->loadPage($this->url . $page_url);     // set next page from paginator array
                }else {
                    // TODO:: check current paginator url
                    $this->paginator = $this->page->loadPaginator();           // get all paginator links on current page
                }
            }
        }
        
        return $this->entities;
    }
    
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Parses provided url
     * @param string $url
     * @return \simple_html_dom
     * @throws ErrorException
     */
    protected function parseUrl(string $url)
    {
        $html = @$this->parser->file_get_html($url);                // Errors silenced
        if (empty($html)) {
            throw new ErrorException('Unable to parse provided url');
        }else {
            return $html;
        }
    }

    /**
     * Loads parsed url into Page Model object
     * @param $url
     * @return Page Model Object
     */
    protected function loadPage($url)
    {
        $parser_url = $this->parseUrl($url);                        // parse url data 
        $page = Page::getInstance($this, $parser_url, $url);        // get new instance of Page object

        return $page;
    }

    /**
     * while       count($this->entities) < ENT_LIMIT      loads entities objects from current page into $this->entities array
     */
    protected function loadEntities()
    {
        $entities_urls = $this->page->getEntitiesUrls();                // Load urls of entities to be loaded
        
        while (count($this->entities) < ENT_LIMIT && $entity_url = next($entities_urls)) {
            fwrite(STDOUT, "\r". "Loading Data: " . $this->getProgress());
            $this->page = $this->loadPage($this->url . $entity_url);    // load new entity page
            $this->setEntity($this->page->getEntity());                 // gets entity object and save it into $this->entities array
        }
        
        return;
    }

    /**
     * Saves entities objects into site entities array
     * @param Entity $entity
     * @throws ErrorException
     */
    protected function setEntity(Entity $entity)
    {
        if($entity instanceof Entity) {
            $this->entities[] = $entity;
            return;
        }else {
            throw new ErrorException('Object is not instance of class Entity');
        }
    }

    /**
     * Returns entities parsing progress in percents
     * @return string
     */
    protected function getProgress()
    {
        return (count($this->entities) / ENT_LIMIT * 100) . "%";
    }

}