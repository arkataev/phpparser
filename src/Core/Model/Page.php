<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 30.04.2016
 */

namespace Core\Model;


abstract class Page
{

    /**
     * @var
     */
    protected $current_url;


    /**
     * @var array   $entities_urls  Holds parsed urls to each Entity on current page
     */
    protected $entities_urls = array();


    /**
     * @var
     */
    protected $contents;

    /**
     * @return mixed
     */
    abstract public function getEntity();


    public function __construct($contentes, $current_url)
    {
        $this->current_url = $current_url;
        $this->contents = $contentes;
    }

    /**
     * @param $site
     * @param \simple_html_dom $parsed_page
     * @param $current_url
     * @return object
     */
    public static function getInstance($site, \simple_html_dom $parsed_page, $current_url)
    {
        $site = str_replace('Site', '', explode('\\',get_class($site))[2]);
        $class = '\\Model\\Page\\'.$site . 'Page';
        
        return new $class($parsed_page, $current_url);
    }

    /**
     * @return array
     */
    public function loadPaginator()
    {
        $paginator_node = $this->getPaginator();
        $paginator[] = $this->current_url;                //  add current page to paginator array for proper counting
        
        foreach ($paginator_node as $page) {
            $paginator[] = preg_replace('/.+\.[a-z]*\//', '/', $page->attr['href'] ? $page->attr['href'] : NULL);
        }

        return $paginator;
    }

    /**
     * @return array
     */
    public function getEntitiesUrls()
    {
        $this->entities_urls = $this->loadEntitiesUrls($this->getEntitiesNodes());

        return $this->entities_urls;
    }

    /**
     * @param string $tag
     * @param int $limit
     * @return mixed
     */
    protected function getNode(string $tag, $limit = 0)
    {
        return $this->contents->find($tag, $limit);
    }

    /**
     * @param string $tag
     * @param int $limit
     * @return null
     */
    protected function getNodeText(string $tag, $limit = 0)
    {
        return $this->contents->find($tag, $limit) ? trim($this->contents->find($tag, $limit)->find('text', $limit)->innertext) : NULL;
    }

    /**
     * @param $entities_nodes
     * @return array
     */
    protected function loadEntitiesUrls($entities_nodes)
    {
        $entities_urls = [];
        foreach ($entities_nodes as $node) {
            if (in_array('href', $node->attr)){
                $entities_urls[] = preg_replace('/.+\.[a-z]+\//', '', $node->attr['href']);
            }else {
                continue;
            }
        }

        return $entities_urls;
    }

    /**
     * @return mixed
     */
    abstract protected function getPaginator();

    /**
     * @return mixed
     */
    abstract protected function getEntitiesNodes();
    
}