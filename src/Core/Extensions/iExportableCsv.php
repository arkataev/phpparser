<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 29.04.2016
 */

namespace Core\Extensions;


interface iExportableCsv
{
    function getCsvColumns();

    function getCsvRow();
}