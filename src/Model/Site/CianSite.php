<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 30.04.2016
 */

namespace Model\Site;

use Core\Model\Site;

class CianSite extends Site
{
    protected $url = 'http://www.cian.ru/';
    protected $sections = array(
        'buy' => array(
            'flat'  => 'kupit-kvartiru',
            'estate' => 'kupit-zagorodnuyu-nedvizhimost',
            'commercial' => 'kupit-pomeshenie',
        ),
        'rent' => array(
            'flat' => 'snyat-kvartiru',
            'estate' => 'snyat-zagorodnuyu-nedvizhimost',
            'commercial'=>'snyat-pomeshenie',
            '24h'=>'snyat-kvartiru-posutochno',
        )
    );

}