<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 30.04.2016
 */

namespace Model\Page;

use Core\Model\Page;
use Core\Model\Entity;

class CianPage extends Page
{
    
    /**
     * @return mixed
     */
    protected function getEntitiesNodes()
    {
        return $this->getNode('.serp-list')->children();        
    }
    
    public function getPaginator()
    {
        return $this->contents->find('.pager_pages', 0)->find('a');
    }

    /**
     * Gets date the Entity was posted on site and converts it into timestamp
     * @return      mixed       if date == 'today' || 'yesterday' returns timestamp, else returns string with date as is.
     */
    protected function parseEntityDate()
    {
        return  $this->getNodeText('.object_descr_dt_added');
    }

    protected function parseEntityAddress()
    {
        return array(
            'address' => $this->getNode('.object_descr_addr')->children(),
            'metro' => $this->getNodeText('.object_descr_metro a'),
        );
    }

    protected function parseEntityProperties()
    {
        return $this->getNode('.object_descr_props tr', NULL);
    }

    protected function parseEntityPrice()
    {
        return $this->getNode('#price_rur', NULL);
    }

    protected function parseEntityType()
    {
        $ent_type = $this->getNodeText('.object_descr_title') . ($this->getNodeText('.object_descr_title a') ? $this->getNodeText('.object_descr_title a') : NULL);
        $ent_type = html_entity_decode(preg_replace('/(\:|&nbsp;)|(\s+)/', ' ', $ent_type));

        return $ent_type;
    }
    
    protected function parseEntityDescription()
    {
        return $this->getNodeText('.object_descr_text');
    }
    
    protected function parseAgentData()
    {
        return array(
            'title' => $this->getNodeText('.realtor-card__title a') ? $this->getNodeText('.realtor-card__title a') : NULL,
            'phone' => $this->getNodeText('.realtor-card__phone a') ? $this->getNodeText('.realtor-card__phone a') : NULL
        );
    }
    
    /**
     * @return CianEntity
     */
    public function getEntity()
    {
        $entity_data = array(
            'ent_type' => $this->parseEntityType(),
            'pub_date' => $this->parseEntityDate(),
            'deal_type'=> 1,
            'address_data' => $this->parseEntityAddress(),
            'properties' => $this->parseEntityProperties(),
            'description' => $this->parseEntityDescription(),
            'price_data' => $this->parseEntityPrice(),
            'agent_data' =>  $this->parseAgentData(),
            'url' => $this->current_url
        );

        return Entity::getInstance($entity_data, $this);
    }





}