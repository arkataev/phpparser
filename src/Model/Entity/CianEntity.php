<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 28.04.2016
 */

namespace Model\Entity;

use Core\Extensions\ExportCSV;
use Core\Model\Entity;
use Core\Extensions\iExportableCsv;

/**
 * Class CianEntity
 * @package Model
 */
class CianEntity extends Entity implements iExportableCsv
{
    use ExportCSV;
    
    /**
     * Entity type
     *
     * @var $ent_type
     */
    private $ent_type;

    /**
     * @var
     */
    private $pub_date;

    /**
     * @var
     */
    private $deal_type;

    /**
     * @var array
     */
    private $address_data = array();

    /**
     * @var
     */
    private $properties;

    /**
     * @var
     */
    private $description;

    /**
     * @var array
     */
    private $price_data = array();

    /**
     * @var array
     */
    private $agent_data = array();

    /**
     * @var
     */
    private $url;


    /**
     * CianEntity constructor.
     * @param $data     array       
     */
    public function __construct($data)
    {
        $this->ent_type = $data['ent_type'];
        $this->pub_date = $this->setDate($data['pub_date']);
        $this->deal_type = $data['deal_type'];
        $this->address_data = $this->setAddress($data['address_data']);
        $this->properties = $this->setProperties($data['properties']);
        $this->description = $data['description'];
        $this->price_data = $this->setPrice($data['price_data']);
        $this->agent_data = $data['agent_data'];
        $this->url = $data['url'];
    }
    
    
    private function setDate($data)
    {
        if (preg_match('/сегодня*,/', $data)){
            $date = time();
        }elseif (preg_match('/вчера*,/', $data)){
            $date = time() - 86400;
        }else {
            $date = $data;
        }
        
        return preg_replace('/.*,.*/', $date, $data);
    }
    
    private function setAddress($data)
    {
        return array(
            'city' => isset($data['address'][0]) ? $data['address'][0]->find('text',0)->innertext : NULL,
            'district' => isset($data['address'][1]) ? $data['address'][1]->find('text',0)->innertext : NULL,
            'street' => isset($data['address'][2]) ? $data['address'][2]->find('text',0)->innertext : NULL,
            'building' => isset($data['address'][3]) ? $data['address'][3]->find('text',0)->innertext : NULL,
            'metro' => isset($data['metro']) ? $data['metro'] : NULL,
        );
    }
    
    
    private function setProperties($data)
    {
        $properties = array();
        
        foreach ($data as $item) {
            $header = $item->find('text', 1)->innertext;
            $text_fields = $item->find('text');
            $text = current(array_filter($text_fields, function($item) {
                return $item->parent()->tag == 'td' && (!empty(preg_replace('/\s+/', "", $item->innertext)));
            }));
            $properties[] = array(
                'header'=>  mb_strtolower(preg_replace('/(\:|&nbsp;)/', '', $header)),
                'text' => preg_replace('/\s+/', " ", html_entity_decode($text ? $text->innertext : NULL))
            );
        }

        return $properties;
    }
    
    
    private function setPrice($data)
    {
        $price = current($data);
        $price_data = array(
            'price' => preg_replace('/(\s+)|(<.+>|<\/.+>)/', ' ', html_entity_decode($price->prev_sibling()->innertext)),
            'additional' => preg_replace('/(\s+)|(<.+>|<\/.+>)/', ' ', html_entity_decode($price->next_sibling()->innertext))
        );

        return $price_data;
    }
    

    public function getCsvColumns()
    {
        $columns = [];
        $vars = get_object_vars($this);
        $properties = array_column($vars['properties'], 'header');
        array_walk_recursive($vars, function($value, $key) use (&$properties, &$columns){
            if ($key != 'header' || $key != 'text') {
                $columns[] = $key;
            }else {
                $columns[] = each($properties)['value'];
            }
        });
        
        return $columns;
    }
    
    
    public function getCsvRow()
    {
        
    }
}